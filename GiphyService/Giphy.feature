﻿Feature: Giphy
	Simple function Giphy.com

Scenario: 01 - Getting GIF by ID
	Given I send request for getting GIF with '9pZw57AyqOHy47oSZq' ID
	Then I have gif response with '200' status
	And I get GIF with '9pZw57AyqOHy47oSZq' ID and 'John Candy Reaction GIF by Laff' title

Scenario: 02 - Searching GIFs by keyword
	Given I send request for searching '10' GIFs of 'g' rating
	Then I have set of gifs response with '200' status
	And I get '10' GIFs with 'g' rating