﻿using Context;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TestData;

namespace StepDefinitions
{
    [Binding]
    public class MySteps
    {
        [Given(@"I send request for getting GIF with '(.*)' ID")]
        public void SendRequestForGettingGIFWithID(string id)
        {
            APIContext.GettingGIFWithID(id);
        }

        [Given(@"I send request for searching '(.*)' GIFs of (.*) rating")]
        public void SendRequestForSearchingGIFsByRating(int quantity, string rating)
        {
            APIContext.SendRequestForSearchingGIFByRating(quantity, rating);
        }

        [Then(@"I have gif response with '(.*)' status")]
        public void VerifyGifResponseStatus(int expectedStatus)
        {
            Assert.AreEqual(expectedStatus, APIContext.GetGifRespondStatus(), $"Response status ISN'T {expectedStatus}");
        }

        [Then(@"I have set of gifs response with '(.*)' status")]
        public void VerifySetOfGifsResponseStatus(int expectedStatus)
        {
            Assert.AreEqual(expectedStatus, APIContext.GetSetOfGifsRespondStatus(), $"Response status ISN'T {expectedStatus}");
        }

        [Then(@"I get GIF with '(.*)' ID and '(.*)' title")]
        public void ThenIGetGIFWithIDAndTitle(string id, string title)
        {
            Assert.AreEqual(id, APIContext.GetGifId(), $"GIF ID ISN'T {id}");
            Assert.AreEqual(title, APIContext.GetGifTitle(), $"GIF title ISN'T {title}");
        }


        [Then(@"I get '(.*)' GIFs with '(.*)' rating")]
        public void VerifySetOfGifsForQuantityAndKeyword(int quantity, string rating)
        {
            Assert.AreEqual(quantity, APIContext.GetQuantityOfGigsInResponse(), 
                $"The quantity of gifs ISN'T {quantity}");
            Assert.Multiple(() =>
            {
                foreach (var gif in DataForTests.SearchResponse.Data)
                {
                    Assert.That(gif.Rating, Is.EqualTo(rating), $"NOT all gifs has {rating} rating");
                }
            });
        }

    }
}
