﻿using Context;

namespace TestData
{
    public class DataForTests
    {
        public const string BaseUrl = "https://api.giphy.com/";

        public const string GifUrl = "v1/gifs/";

        public const string SearchUrl = "search";

        public const string ApiKey = "GWa1gyviDZT6LszZKCSM89K5aHFcTlsJ";

        public static ResponseDto GetResponse { get; set; }

        public static SearchResponseDto SearchResponse { get; set; }
    }
}
