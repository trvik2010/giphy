﻿using System;
using System.Net.Http;
using TestData;
using Newtonsoft.Json;

namespace Context
{
    public class APIContext
    {
        private static readonly HttpClient client = new HttpClient();
        public static void GettingGIFWithID(string id)
        {
            #region Data

            var url = DataForTests.BaseUrl + DataForTests.GifUrl + id + "?api_key=" + DataForTests.ApiKey + "&gif_id=" + id;
            var response = client.GetAsync(new Uri(url)).Result;

            #endregion

            var responseData = response.Content.ReadAsStringAsync().Result;

            DataForTests.GetResponse = JsonConvert.DeserializeObject<ResponseDto>(responseData);
        }

        public static int GetGifRespondStatus() => DataForTests.GetResponse.Meta.Status;
        public static int GetSetOfGifsRespondStatus() => DataForTests.SearchResponse.Meta.Status;

        public static string GetGifId() => DataForTests.GetResponse.Data.Id;

        public static string GetGifTitle() => DataForTests.GetResponse.Data.Title;

        public static void SendRequestForSearchingGIFByRating(int quantity, string rating)
        {
            #region Data

            var url = DataForTests.BaseUrl + DataForTests.GifUrl + DataForTests.SearchUrl +
                "?api_key=" + DataForTests.ApiKey + "&q=sticker&limit=" + quantity + "&rating=" + rating;

            var response = client.GetAsync(new Uri(url)).Result;

            #endregion

            var responseData = response.Content.ReadAsStringAsync().Result;

            DataForTests.SearchResponse = JsonConvert.DeserializeObject<SearchResponseDto>(responseData);
        }

        public static int GetQuantityOfGigsInResponse() => DataForTests.SearchResponse.Pagination.Count;
    }
}
